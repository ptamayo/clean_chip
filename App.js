import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, AsyncStorage, Input, DatePickerIOS, } from 'react-native';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0,
      chosenDate: new Date()
    }
    this.goodButtonPressed = this.goodButtonPressed.bind(this);
    this.badButtonPressed = this.badButtonPressed.bind(this);
    this.setDate = this.setDate.bind(this);

    this._retrieveData()
  }

  _setCounter = async () => {
    try {
      await AsyncStorage.setItem('dayCount', "0");
    } catch (error) {
      console.log("hmm");
    }
  }

  _saveCounter = async () => {
    try {
      await AsyncStorage.setItem('dayCount', String(this.state.counter));
    } catch (error) {
      console.log("hmm");
    }
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('dayCount');
      if (value !== null) {
        console.log(value);
        this.setState(previousState => {
          console.log("setting that ish");
          return { counter: value }
        })
      } else {
        console.log("Initial load, counter doesn't exist");
        this._setCounter()
      }
     } catch (error) {
       console.log("Couldn't retrieve");
     }
  }

  goodButtonPressed(){
    console.log("Button pressed");
    this.setState(previousState => {
      return { counter: Number(previousState.counter) + 1 }
    }, ()=> this._saveCounter())
  }

  badButtonPressed(){
    console.log("bummer");
    this.setState(previousState => {
      return { counter: 0 }
    }, ()=> this._saveCounter())
  }

  setDate(newDate) {
    // console.log(this.state);
    this.setState({chosenDate: newDate})
  }

  render() {
    
    return (
      <View style={styles.container}>
        <Text>When should we say hey?</Text>
        <DatePickerIOS
          date={this.state.chosenDate}
          onDateChange={this.setDate}
          mode="time"
          minuteInterval={10}
        />
      </View>
    )


    return (
      <View style={styles.container}>
        <Text style={styles.counter}>{this.state.counter}</Text>
        <Button
          onPress={this.goodButtonPressed}
          title="Good"
        />
        <Button
          onPress={this.badButtonPressed}
          title="Not so good"
        />
      </View>
    );
  }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center'
//   },
// })

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  counter: {
    justifyContent: 'center',
    color: 'gray',
    fontWeight: 'bold',
    fontSize: 100,
  }
});
